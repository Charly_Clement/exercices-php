<!DOCTYPE html>
<!-- Exercice PHP - CodeColliders 2020 - https://codecolliders.com -->
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

    <?php

    $unNombreAleatoire = mt_rand(0, 50);

    // écrire le code php utilisant les conditions permettant d'afficher un <h2> contenant le texte
    // "la valeur de la variable est supérieur à 25" ou
    // "la valeur de la variable est inférieur à 25" en fonction de la valeur de la variable $unNombreAleatoire
    ?>
    <!-- écrire le code après ce commentaire -->
    <?php
        echo "Nombre " . $unNombreAleatoire . "<br>" . "<br>";
        if ($unNombreAleatoire > 25) {
            echo "<h2> la valeur de la variable est supérieur à 25 </h2>";
        }
            else {
                echo "<h2> la valeur de la variable est inférieur à 25 </h2>";
            }
    ?>

    <!-- écrire le code avant ce commentaire -->

</body>
</html>
