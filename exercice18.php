<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<?php

// Ecrire le code permettant d'afficher l'age de la personne, et avec la fonction Switch, lorsque l'âge franchit la dizaine, 
// afficher la correspondance (60 = Sexagénaire...) allant de 10 à 100 ans. 
// Si l'age n'est pas rond, afficher "Votre âge n'est pas rond".

$age = rand(0, 100);
    
?>
    
<!-- écrire le code avant ce commentaire -->
<?php
    echo $age . "<br>";
    switch ($age) {
        case 0:
            echo "T'es pas né";
        break;
        case 10:
            echo "La dizaine";
            break;
        case 20:
            echo "La vingtaine";
        break;
        case 30:
            echo "La trentaine";
        break;
        case 40:
            echo "La quarantaine";
        break;
        case 50:
            echo "La cinquantaine";
            break;
        case 60:
            echo "La soixantaine";
        break;
        case 70:
            echo "La soixantaine dizaine";
        break;
        case 80:
            echo "La quarantaine vingtaine";
        break;
        case 90:
            echo "La quatrevingt dizaine";
            break;
        case 100:
            echo "Ce n'est qu'un Au revoir";
        break;
        default:
            echo "Votre age n'est pas rond";
    }

?>

<!-- écrire le code avant ce commentaire -->

</body>
</html>
