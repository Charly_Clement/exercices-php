<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<?php

// Ecrire le code permettant d'afficher un message lorsque : 
// L'age de l'utilisateur est compris entre 0 et 17 ans : Trop jeune bonhomme
// L'age de l'utilisateur est compris entre 18 et 21 ans : Majeur mais pas adulte
// L'age de l'utilisateur est compris entre 22 et 55 ans : Adulte et apte au travail
// L'utilisateur à plus de 55 ans : Courage mon cher, la retraite arrive.
// Si l'age est supérieur à 62 ans : Bonne retraite
    
// Dans la même condition, suivant si c'est un homme ou une femme, le texte changera : 
// 0 - 18 ans : Trop jeune, jeune fille
// L'utilisatrice à plus de 55 ans : Courage madame, la retraite arrive.
    
// Si c'est une femme, que son age est compris entre 18 et 40 ans, elle peux avoir un congé maternité

$genre = "Femme";
$age = rand(0, 70);
?>
<!-- écrire le code après ce commentaire -->
<h2>
<?php
    echo $age . "<br>";

    if ($genre == "Femme") {
        if ($age <= 18){
            echo "<br>" . "trop jeune, jeune fille";
        }elseif ($age >= 18 && $age <= 21) {
            echo "<br>" . "Majeure mais pas adulte";
        }elseif ($age >= 22 && $age <= 55) {
            echo "<br>" . "Adulte et apte au travail";
        }elseif ($age >= 18 && $age <= 40) {
            echo "<br>" . "Vous pouvez avoir un congé maternité";
        }elseif ($age >= 55 && $age <=62) {
            echo "<br>" . "Courage madame, la retraite arrive.";
        }else {
            echo "<br>" . "Bonne retraite";
        }
        
    }else {
        if ($age <= 18){
        echo "<br>" . "trop jeune bonhomme";
        }elseif ($age >= 18 && $age <= 21) {
            echo "<br>" . "Majeur mais pas adulte";
        }elseif ($age >= 22 && $age <= 55) {
            echo "<br>" . "Adulte et apte au travail";
        }elseif ($age >= 55 && $age <62) {
            echo "<br>" . "Courage monsieur, la retraite arrive.";
        }else {
            echo "<br>" . "Bonne retraite";
        }
        }
    
?>
</h2>
<!-- écrire le code avant ce commentaire -->

</body>
</html>